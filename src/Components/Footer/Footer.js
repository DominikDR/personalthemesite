import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-regular-svg-icons';
import {
  faFacebookF,
  faTwitter,
  faDribbble,
  faBehance,
} from '@fortawesome/free-brands-svg-icons';
import { NewsletterInput } from './NewsletterInput';

const Container = styled.div`
  background-color: #04091e;
  display: flex;
  justify-content: center;
  max-width: 100%;
`;

const Wrapper = styled.div`
  width: ${props => props.width || '450px'};
  padding: 120px 12px 120px;
`;

const Title = styled.div`
  font-size: 18px;
  color: #fff;
  padding-bottom: 8px;
`;

const Content = styled.p`
  color: #777777;
  line-height: 1.6rem;
`;

const SocialIcons = styled.div`
  color: #ffffff;
  & > svg {
    padding: 0 8px;
    line-height: 1.6rem;
    cursor: pointer;

    &:hover {
      color: #8490ff;
    }
  }
`;

const HomeLink = styled.a`
  color: #8490ff;
  text-decoration: none;
`;

const Footer = () => {
  return (
    <Container>
      <Wrapper>
        <Title>About Me</Title>
        <Content>
          We have tested a number of registry fix and clean utilities and
          present our top 3 list on our site for your convenience.
        </Content>
        <Content>
          Copyright ©{new Date().getFullYear()} All rights reserved | This
          template is made with <FontAwesomeIcon icon={faHeart} /> by
          <HomeLink href="/"> me</HomeLink>
        </Content>
      </Wrapper>
      <Wrapper>
        <Title>Newsletter</Title>
        <Content>Stay updated with our latest trends</Content>
        <NewsletterInput />
      </Wrapper>
      <Wrapper width="fit-content">
        <Title>Follow Me</Title>
        <Content>Let us be social</Content>
        <SocialIcons>
          <FontAwesomeIcon icon={faFacebookF} />
          <FontAwesomeIcon icon={faTwitter} />
          <FontAwesomeIcon icon={faDribbble} />
          <FontAwesomeIcon icon={faBehance} />
        </SocialIcons>
      </Wrapper>
    </Container>
  );
};

export { Footer };
