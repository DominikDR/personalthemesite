import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { NewsletterInput } from './NewsletterInput';

describe('NewsletterInput', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<NewsletterInput />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
