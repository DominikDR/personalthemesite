import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
`;

const Input = styled.input`
  color: #222222;
  width: 365px;
  height: 28px;
  font-size: 14px;
  padding: 6px 12px 6px 17px;
  outline: none;

  &::selection {
    color: #777777;
    background-color: #191919;
  }
`;

const Button = styled.button`
  height: 44px;
  width: 44px;
  border: none;
  cursor: pointer;
  background-color: #8490ff;

  &:focus {
    outline: none;
    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
  }
`;

const RightArrow = styled.span`
  font-size: 25px;
  color: #ffffff;
  &:after {
    content: '\\2192';
    vertical-align: super;
  }
`;

const NewsletterInput = () => {
  return (
    <Container>
      <Input placeholder="Enter Email Address" />
      <Button>
        <RightArrow />
      </Button>
    </Container>
  );
};

export { NewsletterInput };
